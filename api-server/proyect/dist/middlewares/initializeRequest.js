"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("../config/config"));
const dataGet_1 = __importStar(require("../core/dataGet"));
class initializeRequest {
    static readDataGET(query) {
        let dataGET = new dataGet_1.default();
        for (let index in query) {
            let value = query[index];
            switch (index) {
                case 'embed':
                    if (value != '') {
                        let embeds = value.split(",");
                        for (let embed of embeds) {
                            let index = dataGET.fields_config.embed.length;
                            dataGET.fields_config.embed[index] = new dataGet_1.FieldsConfigEmbed();
                            dataGET.fields_config.embed[index]['embed'] = embed;
                        }
                    }
                    break;
                case 'sort':
                    if (value != '') {
                        let columns = value.split(",");
                        for (let column of columns) {
                            if (dataGET.list_config.sort == null)
                                dataGET.list_config.sort = [];
                            let car = column.substring(0, 1);
                            if (car == '-') {
                                column = column.replace(car, "");
                                dataGET.list_config.sort.push(new dataGet_1.ListConfigSort(column, 'DESC'));
                            }
                            else {
                                dataGET.list_config.sort.push(new dataGet_1.ListConfigSort(column, 'ASC'));
                            }
                        }
                    }
                    break;
                case 'search':
                    dataGET.list_config.search = value;
                    break;
                case 'page':
                    dataGET.list_config.page = parseInt(value);
                    break;
                case 'limit':
                    let limit = parseInt(value);
                    if (limit > config_1.default.MAX_LIMIT)
                        limit = config_1.default.MAX_LIMIT;
                    dataGET.list_config.limit = limit;
                    break;
                case 'not_limit':
                    let not_limit = parseInt(value);
                    if (not_limit != 0 && not_limit != 1)
                        not_limit = 0;
                    dataGET.list_config.not_limit = not_limit;
                    break;
                default:
                    dataGET.list_config.filters[index] = value;
                    break;
            }
        }
        if (dataGET.list_config.not_limit == 1) {
            dataGET.list_config.limit = -1;
        }
        return dataGET;
    }
}
exports.default = initializeRequest;
initializeRequest.start = (req, res, next) => {
    let dataGET = initializeRequest.readDataGET(req.query);
    req.query.dataGET = dataGET;
    next();
};
