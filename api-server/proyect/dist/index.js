"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = __importDefault(require("./core/server"));
const router_1 = __importDefault(require("./router/router"));
const config_1 = __importDefault(require("./config/config"));
const bodyParser = require("body-parser");
const cors_1 = __importDefault(require("cors"));
const helmet = require('helmet');
const server = server_1.default.instance;
server.app.use(bodyParser.urlencoded({ extended: false }));
server.app.use(bodyParser.json());
server.app.use(cors_1.default({
    origin: '*',
    methods: 'GET, POST, PUT, DELETE, OPTIONS',
    optionsSuccessStatus: 200,
    credentials: true
}));
server.app.use(router_1.default);
server.app.use(helmet());
server.start(() => {
    console.log(`Server started on port ${config_1.default.port}`);
});
