"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const CustomerGroupsController_1 = __importDefault(require("../controllers/CustomerGroupsController"));
const routerCustomerGroups = express_1.Router();
routerCustomerGroups.get('/customer-groups', CustomerGroupsController_1.default.getCustomerGroups);
routerCustomerGroups.post('/customer-groups', CustomerGroupsController_1.default.postCustomerGroup);
routerCustomerGroups.get('/customer-groups/:id', CustomerGroupsController_1.default.getCustomerGroup);
routerCustomerGroups.put('/customer-groups/:id', CustomerGroupsController_1.default.putCustomerGroup);
routerCustomerGroups.delete('/customer-groups/:id', CustomerGroupsController_1.default.deleteCustomerGroup);
exports.default = routerCustomerGroups;
