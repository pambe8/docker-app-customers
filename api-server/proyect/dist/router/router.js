"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const initializeRequest_1 = __importDefault(require("../middlewares/initializeRequest"));
const customers_1 = __importDefault(require("./customers"));
const customer_groups_1 = __importDefault(require("./customer_groups"));
const router = express_1.Router();
router.use('/', initializeRequest_1.default.start, customers_1.default);
router.use('/', initializeRequest_1.default.start, customer_groups_1.default);
exports.default = router;
