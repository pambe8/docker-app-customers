"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const CustomersController_1 = __importDefault(require("../controllers/CustomersController"));
const routerCustomers = express_1.Router();
routerCustomers.get('/customers', CustomersController_1.default.getCustomers);
routerCustomers.post('/customers', CustomersController_1.default.postCustomer);
routerCustomers.get('/customers/:id', CustomersController_1.default.getCustomer);
routerCustomers.put('/customers/:id', CustomersController_1.default.putCustomer);
routerCustomers.delete('/customers/:id', CustomersController_1.default.deleteCustomer);
exports.default = routerCustomers;
