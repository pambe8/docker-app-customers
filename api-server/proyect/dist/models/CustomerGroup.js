"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const BaseModel_1 = __importDefault(require("./BaseModel"));
class CustomerGroup extends BaseModel_1.default {
    constructor(row) {
        super('ta_customer_groups');
        if (row !== undefined) {
            this.id = row.id;
            this.name = row.name;
            this.exists = true;
        }
    }
    findById(id) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let customer = new CustomerGroup();
            let sql = `SELECT 
                *
                from ${this.getTable()}
                where 
                id=${this.getDB().escape(id)}`;
            let results = yield this.getDB().query(sql);
            if (results.length == 1) {
                let data = results[0];
                customer = new CustomerGroup(data);
            }
            resolve(customer);
        }));
    }
    insert() {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            if (this.exists) {
                let sql = `UPDATE ${this.getTable()}
                        SET
                        name=${this.getDB().escape(this.name)}`;
                sql += ` WHERE id=${this.getDB().escape(this.id)}`;
                try {
                    yield this.getDB().query(sql);
                    resolve(true);
                }
                catch (e) {
                    resolve(false);
                }
            }
            else {
                let sql = `INSERT INTO ${this.getTable()} (
                    name
                    )
                     VALUES (
                    ${this.getDB().escape(this.name)}`;
                sql += `)`;
                try {
                    let results = yield this.getDB().query(sql);
                    this.id = results.insertId;
                    this.exists = true;
                    resolve(true);
                }
                catch (e) {
                    resolve(false);
                }
            }
        }));
    }
    delete() {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            if (this.exists) {
                let sql = `UPDATE ta_customers
                        SET
                        id_customer_group=0 WHERE id_customer_group=${this.getDB().escape(this.id)}`;
                yield this.getDB().query(sql);
                sql = `DELETE FROM ${this.getTable()} WHERE id=${this.getDB().escape(this.id)}`;
                try {
                    yield this.getDB().query(sql);
                    resolve(true);
                }
                catch (e) {
                    resolve(false);
                }
            }
            else {
                resolve(false);
            }
        }));
    }
    getCustomerGroups(list_config) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            let total = 0;
            let page = 0;
            let limit = 0;
            let elements = [];
            let customerGroup = new CustomerGroup();
            let sql_count = "select count(id) as total ";
            let sql_rows = "select * ";
            let sql = ` from ${this.getTable()} 
            where 1=1 `;
            if (list_config !== undefined) {
                sql += yield customerGroup.filtersSql(list_config.filters);
                sql_count += sql;
                sql += ` and name like ${this.getDB().escape('%' + list_config.search + '%')} `;
                if (list_config.sort.length > 0)
                    sql += yield customerGroup.sortSql(list_config.sort);
                else
                    sql += ` ORDER BY name ASC `;
                sql += customerGroup.limitSql(list_config);
                page = list_config.page;
                limit = list_config.limit;
            }
            else {
                sql_count += sql;
                sql += ` ORDER BY name ASC `;
            }
            sql_rows += sql;
            let results = yield this.getDB().query(sql_count);
            total = results[0].total;
            results = yield this.getDB().query(sql_rows);
            for (let data of results) {
                elements.push(new CustomerGroup(data));
            }
            resolve({
                total,
                elements,
                page,
                limit
            });
        }));
    }
}
exports.default = CustomerGroup;
