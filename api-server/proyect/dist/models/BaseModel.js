"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql_1 = __importDefault(require("../core/mysql"));
const dataGet_1 = __importDefault(require("../core/dataGet"));
const util_1 = require("util");
class BaseModel {
    constructor(table) {
        this.exists = false;
        this.privateColumns = [];
        this.columns = [];
        this.embed = {};
        this.type_element_images = 0;
        this.table = table;
        this.db = mysql_1.default.instance;
    }
    getTable() {
        return this.table;
    }
    initializeColumns() {
        return new Promise((resolve, reject) => {
            let sql = `SHOW COLUMNS FROM ${this.table}`;
            this.db.query(sql, true).then(results => {
                for (let data of results) {
                    let index = this.privateColumns.indexOf(data['Field']);
                    if (index == -1) {
                        this.columns.push(data['Field']);
                    }
                }
                resolve(true);
            }).catch(() => {
                reject(false);
            });
        });
    }
    getColumns() {
        return new Promise((resolve, reject) => {
            if (this.columns.length == 0) {
                this.initializeColumns().then(() => {
                    resolve(this.columns);
                }).catch(() => {
                    reject(false);
                });
            }
            else {
                resolve(this.columns);
            }
        });
    }
    getDB() {
        return this.db;
    }
    getExists() {
        return this.exists;
    }
    setPrivateColumn(column) {
        this.privateColumns.push(column);
    }
    setEmbed(embed, method) {
        this.embed[embed] = method;
    }
    toObject(fields_config) {
        return new Promise((resolve, reject) => {
            let object = {};
            let objectThis = this;
            this.getColumns().then((columns) => __awaiter(this, void 0, void 0, function* () {
                for (let column of columns) {
                    object[column] = objectThis[column];
                }
                if (fields_config && fields_config.embed.length > 0) {
                    for (let embed of fields_config.embed) {
                        if (this.embed[embed.embed] !== undefined) {
                            let dataGET = new dataGet_1.default();
                            let result = yield objectThis[this.embed[embed.embed]]();
                            if (util_1.isArray(result)) {
                                object[embed.embed] = [];
                                for (let i = 0; i < result.length; i++) {
                                    let objResult = yield result[i].toObject(dataGET.fields_config);
                                    object[embed.embed].push(objResult);
                                }
                            }
                            else if (typeof result === "object") {
                                if (result.total !== undefined && result.elements !== undefined) {
                                    object[embed.embed] = [];
                                    for (let i = 0; i < result.elements.length; i++) {
                                        let objResult = yield result.elements[i].toObject(dataGET.fields_config);
                                        object[embed.embed].push(objResult);
                                    }
                                }
                                else {
                                    let objResult = yield result.toObject(dataGET.fields_config);
                                    object[embed.embed] = objResult;
                                }
                            }
                            else {
                                object[embed.embed] = result;
                            }
                        }
                    }
                }
                resolve(object);
            })).catch(() => {
                reject(false);
            });
        });
    }
    filtersSql(filters, abbreviationTable = '') {
        return new Promise((resolve, reject) => {
            if (abbreviationTable != '')
                abbreviationTable += '.';
            let sql = '';
            this.getColumns().then(columns => {
                for (const prop in filters) {
                    let indexColumn = columns.indexOf(prop);
                    if (indexColumn != -1) {
                        sql += ` and ${abbreviationTable}${columns[indexColumn]}=${this.db.escape(filters[prop])} `;
                    }
                }
                resolve(sql);
            }).catch(() => {
                reject(false);
            });
            ;
        });
    }
    sortSql(sorts, abbreviationTable = '') {
        return new Promise((resolve, reject) => {
            if (abbreviationTable != '')
                abbreviationTable += '.';
            let sql = '';
            this.getColumns().then(columns => {
                for (let sort of sorts) {
                    let indexColumn = columns.indexOf(sort.column);
                    if (indexColumn != -1) {
                        switch (sort.direction) {
                            case 'ASC':
                                if (sql != '')
                                    sql += ', ';
                                sql = `${sql}${abbreviationTable}${columns[indexColumn]} ASC`;
                                break;
                            case 'DESC':
                                if (sql != '')
                                    sql += ', ';
                                sql = `${sql}${abbreviationTable}${columns[indexColumn]} ASC`;
                                break;
                        }
                    }
                }
                if (sql != '') {
                    sql = ` ORDER BY ${sql} `;
                }
                resolve(sql);
            }).catch(() => {
                reject(false);
            });
            ;
        });
    }
    limitSql(config) {
        let sql = '';
        if (config.not_limit == 0) {
            let offset = config.page * config.limit;
            sql = `LIMIT ${this.db.escape(offset)}, ${this.db.escape(config.limit)}`;
        }
        return sql;
    }
}
exports.default = BaseModel;
