"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("../config/config"));
class DataGet {
    constructor() {
        this.fields_config = new FieldsConfig();
        this.list_config = new ListConfig();
    }
}
exports.default = DataGet;
class ListConfig {
    constructor() {
        this.filters = {};
        this.sort = [];
        this.search = '';
        this.page = 0;
        this.limit = config_1.default.MAX_LIMIT;
        this.not_limit = 0;
    }
}
exports.ListConfig = ListConfig;
class ListConfigSort {
    constructor(column, direction) {
        this.column = column;
        this.direction = direction;
    }
}
exports.ListConfigSort = ListConfigSort;
class FieldsConfig {
    constructor() {
        this.embed = [];
    }
}
exports.FieldsConfig = FieldsConfig;
class FieldsConfigEmbed {
    constructor() {
        this.embed = '';
    }
}
exports.FieldsConfigEmbed = FieldsConfigEmbed;
