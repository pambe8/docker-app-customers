"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql = require("mysql");
const config_1 = __importDefault(require("../config/config"));
const xss = require("xss");
class MySQL {
    constructor() {
        this.cnn = mysql.createPool({
            connectionLimit: config_1.default.connection_db.connection_limit,
            host: config_1.default.connection_db.host,
            user: config_1.default.connection_db.user,
            password: config_1.default.connection_db.password,
            database: config_1.default.connection_db.database,
            dateStrings: true,
            charset: config_1.default.connection_db.charset
        });
        this.cnn.getConnection((err, connection) => {
            if (err) {
                if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                    console.error('Database connection was closed.');
                }
                if (err.code === 'ER_CON_COUNT_ERROR') {
                    console.error('Database has too many connections.');
                }
                if (err.code === 'ECONNREFUSED') {
                    console.error('Database connection was refused.');
                }
            }
            if (connection)
                connection.release();
            return;
        });
    }
    escape(data) {
        data = this.cnn.escape(data);
        data = xss(data);
        return data;
    }
    static get instance() {
        return this._instance || (this._instance = new this());
    }
    query(query, cache = false) {
        return new Promise((resolve, reject) => {
            this.cnn.query(query, (error, results, fields) => {
                if (error) {
                    console.log(error);
                    reject(error);
                }
                else {
                    resolve(results);
                }
            });
        });
    }
}
exports.default = MySQL;
