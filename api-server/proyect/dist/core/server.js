"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const http_1 = __importDefault(require("http"));
const mysql_1 = __importDefault(require("../core/mysql"));
const config_1 = __importDefault(require("../config/config"));
class Server {
    constructor() {
        this.port = config_1.default.port;
        this.app = express();
        this.httpServer = new http_1.default.Server(this.app);
    }
    static get instance() {
        return this._instance || (this._instance = new this());
    }
    start(callback) {
        mysql_1.default.instance;
        this.httpServer.listen(this.port, callback);
    }
}
exports.default = Server;
