"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Config {
    constructor() {
        this.config = require('./config.json');
    }
    static get instance() {
        return this._instance || (this._instance = new this());
    }
    getConfig() {
        return this.config;
    }
}
let config = Config.instance.getConfig();
exports.default = config;
