"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const CustomerGroup_1 = __importDefault(require("../models/CustomerGroup"));
class CustomerGroupsController {
}
exports.default = CustomerGroupsController;
CustomerGroupsController.getCustomerGroups = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let dataGET = req.query.dataGET;
    let objCustomerGroup = new CustomerGroup_1.default();
    let result = yield objCustomerGroup.getCustomerGroups(dataGET.list_config);
    let data = [];
    for (let element of result.elements) {
        data.push(yield element.toObject(dataGET.fields_config));
    }
    res.json({
        status: true,
        data,
        total: result.total,
        page: result.page,
        limit: result.limit
    });
});
CustomerGroupsController.getCustomerGroup = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let dataGET = req.query.dataGET;
    let id_customer_group = parseInt(req.params.id);
    let objCustomerGroup = new CustomerGroup_1.default();
    objCustomerGroup = yield objCustomerGroup.findById(id_customer_group);
    if (objCustomerGroup.getExists()) {
        let element = yield objCustomerGroup.toObject(dataGET.fields_config);
        res.json({
            status: true,
            element
        });
    }
    else {
        res.json({
            status: false,
            error: 'not exists'
        });
    }
});
CustomerGroupsController.postCustomerGroup = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let name = req.body.name;
    let objCustomerGroup = new CustomerGroup_1.default();
    objCustomerGroup.name = name;
    objCustomerGroup.insert().then((status) => __awaiter(void 0, void 0, void 0, function* () {
        if (status) {
            let element = yield objCustomerGroup.toObject();
            res.json({
                status: true,
                element
            });
        }
        else {
            res.json({
                status: false,
                error: 'Error'
            });
        }
    }));
});
CustomerGroupsController.putCustomerGroup = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let name = req.body.name;
    let id_customer_group = parseInt(req.params.id);
    let objCustomerGroup = new CustomerGroup_1.default();
    objCustomerGroup = yield objCustomerGroup.findById(id_customer_group);
    if (objCustomerGroup.getExists()) {
        objCustomerGroup.name = name;
        objCustomerGroup.insert().then((status) => __awaiter(void 0, void 0, void 0, function* () {
            if (status) {
                let element = yield objCustomerGroup.toObject();
                res.json({
                    status: true,
                    element
                });
            }
            else {
                res.json({
                    status: false,
                    error: 'Error'
                });
            }
        }));
    }
    else {
        res.json({
            status: false,
            error: 'not exists'
        });
    }
});
CustomerGroupsController.deleteCustomerGroup = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let id_customer_group = parseInt(req.params.id);
    let objCustomerGroup = new CustomerGroup_1.default();
    objCustomerGroup = yield objCustomerGroup.findById(id_customer_group);
    if (objCustomerGroup.getExists()) {
        objCustomerGroup.delete().then((status) => __awaiter(void 0, void 0, void 0, function* () {
            if (status) {
                res.json({
                    status: true
                });
            }
            else {
                res.json({
                    status: false,
                    error: 'Error'
                });
            }
        }));
    }
    else {
        res.json({
            status: false,
            error: 'not exists'
        });
    }
});
