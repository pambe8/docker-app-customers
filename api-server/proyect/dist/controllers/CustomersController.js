"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Customer_1 = __importDefault(require("../models/Customer"));
class CustomersController {
}
exports.default = CustomersController;
CustomersController.getCustomers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let dataGET = req.query.dataGET;
    let objCustomer = new Customer_1.default();
    let result = yield objCustomer.getCustomers(dataGET.list_config);
    let data = [];
    for (let element of result.elements) {
        data.push(yield element.toObject(dataGET.fields_config));
    }
    res.json({
        status: true,
        data,
        total: result.total,
        page: result.page,
        limit: result.limit
    });
});
CustomersController.getCustomer = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let dataGET = req.query.dataGET;
    let id_customer = parseInt(req.params.id);
    let objCustomer = new Customer_1.default();
    objCustomer = yield objCustomer.findById(id_customer);
    if (objCustomer.getExists()) {
        let element = yield objCustomer.toObject(dataGET.fields_config);
        res.json({
            status: true,
            element
        });
    }
    else {
        res.json({
            status: false,
            error: 'not exists'
        });
    }
});
CustomersController.postCustomer = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let name = req.body.name;
    let description = req.body.description;
    let id_customer_group = req.body.id_customer_group;
    let objCustomer = new Customer_1.default();
    objCustomer.name = name;
    objCustomer.description = description;
    objCustomer.id_customer_group = id_customer_group;
    objCustomer.insert().then((status) => __awaiter(void 0, void 0, void 0, function* () {
        if (status) {
            let element = yield objCustomer.toObject();
            res.json({
                status: true,
                element
            });
        }
        else {
            res.json({
                status: false,
                error: 'Error'
            });
        }
    }));
});
CustomersController.putCustomer = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let name = req.body.name;
    let description = req.body.description;
    let id_customer_group = req.body.id_customer_group;
    let id_customer = parseInt(req.params.id);
    let objCustomer = new Customer_1.default();
    objCustomer = yield objCustomer.findById(id_customer);
    if (objCustomer.getExists()) {
        objCustomer.name = name;
        objCustomer.description = description;
        objCustomer.id_customer_group = id_customer_group;
        objCustomer.insert().then((status) => __awaiter(void 0, void 0, void 0, function* () {
            if (status) {
                let element = yield objCustomer.toObject();
                res.json({
                    status: true,
                    element
                });
            }
            else {
                res.json({
                    status: false,
                    error: 'Error'
                });
            }
        }));
    }
    else {
        res.json({
            status: false,
            error: 'not exists'
        });
    }
});
CustomersController.deleteCustomer = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let id_customer = parseInt(req.params.id);
    let objCustomer = new Customer_1.default();
    objCustomer = yield objCustomer.findById(id_customer);
    if (objCustomer.getExists()) {
        objCustomer.delete().then((status) => __awaiter(void 0, void 0, void 0, function* () {
            if (status) {
                res.json({
                    status: true
                });
            }
            else {
                res.json({
                    status: false,
                    error: 'Error'
                });
            }
        }));
    }
    else {
        res.json({
            status: false,
            error: 'not exists'
        });
    }
});
